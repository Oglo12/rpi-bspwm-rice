#!/usr/bin/env bash

killstart () {
	killall $1
	pkill $1
	sleep 0.1
	$1 ${@:2}
}

lxappearance &
sleep 0.5
killall lxappearance
pkill lxappearance

killstart lxsession &
killstart picom -b --config ~/.config/bspwm/picom/picom.conf
killstart dunst -conf ~/.config/bspwm/dunst/dunstrc

~/.config/bspwm/polybar/start

if [[ -d ~/.cache/nitrogen ]]; then
	nitrogen --restore
else
	nitrogen --set-zoom-fill ~/.config/bspwm/wallpaper.png
fi
