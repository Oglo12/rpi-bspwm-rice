# How To Install

> NOTE: You should run all of these commands using Bash! (Other shells may work, but some like Nushell use very different syntax!)

Install All Required Programs: `curl https://gitlab.com/Oglo12/rpi-bspwm-rice/-/raw/main/required_programs.txt | xargs sudo apt install -y`

Download The Config: `cd ~/.config && git clone https://gitlab.com/Oglo12/rpi-bspwm-rice.git bspwm`

Symlink Directories: `ln -s $HOME/.config/bspwm/rofi $HOME/.config/rofi && ln -s $HOME/.config/bspwm/alacritty $HOME/.config/alacritty`

Download Fonts: `mkdir ~/.fonts; cd ~/.fonts && wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.tar.xz`

Unzip Fonts: `cd ~/.fonts && tar -xvf JetBrainsMono.tar.xz && rm README.md OFL.txt JetBrainsMono.tar.xz`

Regenerate Font Cache: `fc-cache`

After all those steps, just start Bspwm and you should be good to go!

If you notice any issues, please let me know through opening a new issue on the repository! (I get an email notification.)
