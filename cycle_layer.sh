#!/bin/bash

source ~/.config/bspwm/libs.sh

l_mode=$(get_node_layer_info)

layers=("above" "normal" "below")

for i in $(seq 0 $((${#layers[@]} - 1))); do
	if [[ ${layers[$i]} == $l_mode ]]; then
		l_next=$(($i + 1))

		if [[ $l_next == ${#layers[@]} ]]; then
			l_next=0
		fi

		bspc node -l ${layers[$l_next]}
		notify-send "${layers[$l_next]}" -u low
	fi
done
