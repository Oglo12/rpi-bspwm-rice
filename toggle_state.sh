#!/bin/bash

source ~/.config/bspwm/libs.sh

t_mode=$(get_node_tile_info)

if [[ $t_mode == "$1" ]]; then
	bspc node -t tiled
else
	bspc node -t $1
fi
